//
// Created by sharky on 19.09.18.
//

#include "MyTestApp.h"
//#include <Og

#include <OGRE/OgreRoot.h>
#include <OGRE/OgreSceneManager.h>
#include <RTShaderSystem/OgreRTShaderSystem.h>
#include <OgreSceneNode.h>
#include <OgreCamera.h>
#include <OgreRenderWindow.h>
#include <OgreEntity.h>
#include <OgreViewport.h>
#include <OgreColourValue.h>
#include <OgreProfiler.h>




#include <OgreMeshManager.h>

using Ogre::Vector3;
using Ogre::Degree;
using Ogre::Light;
using Ogre::Entity;
using Ogre::SceneNode;
using Ogre::ColourValue;


MyTestApp::MyTestApp() : OgreBites::ApplicationContext("OgreTutorialApp")
{
}



void MyTestApp::setup(void)
{
    // do not forget to call the base first
    OgreBites::ApplicationContext::setup();
    //Ogre::Profiler::getSingleton().setEnabled(false);
    // register for input events
    //addInputListener(this);
    // get a pointer to the already created root
    Ogre::Root* root = getRoot();
    Ogre::SceneManager* scnMgr = root->createSceneManager();
    // register our scene with the RTSS
    Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);
    // without light we would just get a black screen
    Ogre::Light* light = scnMgr->createLight("MainLight");
    Ogre::SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    lightNode->setPosition(0, 400, 100);
    //lightNode->attachObject(light);
    // also need to tell where we are
    Ogre::SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(200, 300, 400);
    camNode->lookAt(Ogre::Vector3(0, 0, 0), Ogre::Node::TransformSpace::TS_WORLD);
    // create the camera
    Ogre::Camera* cam = scnMgr->createCamera("myCam");
    cam->setNearClipDistance(5); // specific to this sample
    cam->setAutoAspectRatio(true);
    camNode->attachObject(cam);
    // and tell it to render into the main window
    Ogre::Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(Ogre::ColourValue(0.3, 0.45, 0.45));

    // finally something to render
    Ogre::Entity* ent = scnMgr->createEntity("ninja.mesh");
    ent->setCastShadows(true);
    Ogre::SceneNode* node = scnMgr->getRootSceneNode()->createChildSceneNode();
    node->attachObject(ent);

    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
    Ogre::MeshManager::getSingleton().createPlane(
            "ground",
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            plane,
            1500,1500,20,20,
            true,
            1,5,5,
            Ogre::Vector3::UNIT_Z
            );
    Ogre::Entity * groundEntity = scnMgr->createEntity("ground");
    scnMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity);
    groundEntity->setCastShadows(false);
    groundEntity->setMaterialName("Examples/Rockwall");

    scnMgr->setAmbientLight(Ogre::ColourValue(0, 0, 0));
    scnMgr->setShadowTechnique(Ogre::ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);
//    scnMgr->setShadowTexturePixelFormat(Ogre::PixelFormat::PF_FLOAT32_R);
//    scnMgr->setShadowTechnique( Ogre::ShadowTechnique::SHADOWTYPE_TEXTURE_ADDITIVE );
//    scnMgr->setShadowTextureCasterMaterial(Ogre::Material("Ogre/DepthShadowmap/Caster/Float"));
//    scnMgr->setShadowTextureReceiverMaterial("Ogre/DepthShadowmap/Receiver/Float");
//    scnMgr->setShadowTextureSelfShadow(true);
//    scnMgr->setShadowTextureSize(1024);

    Ogre::Light* spotLight = scnMgr->createLight("SpotLight");
    spotLight->setDiffuseColour(0, 0, 1.0);
    spotLight->setSpecularColour(0, 0, 1.0);
    spotLight->setType(Ogre::Light::LT_SPOTLIGHT);
    Ogre::SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->attachObject(spotLight);
    spotLightNode->setDirection(-1, -1, 0);
    spotLightNode->setPosition(Vector3(200, 200, 0));
    spotLight->setSpotlightRange(Degree(35), Degree(50));

    Light* directionalLight = scnMgr->createLight("DirectionalLight");
    directionalLight->setType(Light::LT_DIRECTIONAL);

    directionalLight->setDiffuseColour(ColourValue(0.4, 0, 0));
    directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));

    Light* pointLight = scnMgr->createLight("PointLight");
    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);
    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    pointLightNode->attachObject(pointLight);
    pointLightNode->setPosition(Vector3(0, 150, 250));

}