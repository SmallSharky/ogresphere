//
// Created by sharky on 20.09.18.
//

#ifndef OGRESPHERE_CAMMANIPULATOR_H
#define OGRESPHERE_CAMMANIPULATOR_H


#include <Bites/OgreInput.h>


class CamManipulator: public OgreBites::InputListener {
public:

    bool keyPressed(const OgreBites::KeyboardEvent& evt);
    bool mouseMoved(const OgreBites::MouseMotionEvent& evt);

};


#endif //OGRESPHERE_CAMMANIPULATOR_H
