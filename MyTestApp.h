//
// Created by sharky on 19.09.18.
//

#ifndef OGRESPHERE_MYTESTAPP_H
#define OGRESPHERE_MYTESTAPP_H

#include <Bites/OgreApplicationContext.h>


class MyTestApp : public OgreBites::ApplicationContext
{
public:
    MyTestApp();
    void setup(void);
};


#endif //OGRESPHERE_MYTESTAPP_H
