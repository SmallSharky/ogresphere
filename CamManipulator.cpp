//
// Created by sharky on 20.09.18.
//

#include "CamManipulator.h"

#include <iostream>



using std::cout;


bool CamManipulator::keyPressed(const OgreBites::KeyboardEvent& evt)
{
    //cout<<"input event\n";
    if (evt.keysym.sym == OgreBites::SDLK_ESCAPE)
    {
        //getRoot()->queueEndRendering();
    }
    return true;
}

bool CamManipulator::mouseMoved(const OgreBites::MouseMotionEvent &evt) {

    cout<<"Mouse moved "<<evt.xrel<<":"<<evt.yrel<<"\n";
    return true;
}