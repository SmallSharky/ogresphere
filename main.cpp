#include <iostream>

#include "MyTestApp.h"
#include "CamManipulator.h"

#include <OgreRoot.h>

int main() {
    MyTestApp app;
    app.initApp();
    CamManipulator mp;

    app.addInputListener(&mp);
    app.getRoot()->startRendering();
    app.closeApp();
    return 0;
}